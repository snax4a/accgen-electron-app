const isDev = require("electron-is-dev");
if (!isDev && !window.location.hostname.endsWith("cathook.club"))
    return;
const axios = require("axios");
const proxyAgent = require('proxy-agent');
// Full IPC
const ipc = require("electron").ipcRenderer;
const Timeout = require("await-timeout");
const url = require("url");
// Limited IPC for safety
document.ipc = require("./ipc.js")
document.isDev = isDev;

document.toughCookie = require("tough-cookie");
document.proxiedHttpRequest = function (options, proxy, cookies, resolve, reject, timeout) {
    if (!timeout)
        timeout = 30000;
    var agent = undefined;
    if (proxy) {
        var proxyopts = url.parse(proxy);
        if (proxyopts.protocol == "https:")
            proxyopts.protocol = "http:"
        agent = new proxyAgent(proxyopts);
    }
    Timeout.wrap(axios(
        extend({
            httpsAgent: agent,
            httpAgent: agent,
            jar: cookies,
            withCredentials: true,
            proxy: false,
        }, options)), timeout, "Proxied connection timed out!").then(function (res) {
            resolve(res.data);
        }, function (err) {
            console.error(err);
            reject(err.response, err);
        });
}

document.startSteam = function (account) {
    ipc.send("start-steam", account)
}
